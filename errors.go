package protobuf2

import "errors"

var (
	ErrSumInvalid     = errors.New("message sum is invalid")
	ErrUnknownProto   = errors.New("unknown proto")
	ErrTooShort       = errors.New("message too short(<16)")
	ErrDataLenInvalid = errors.New("data len invalid")
)
