package protobuf2

import (
	"gitlab.com/flex_comp/comp"
	"gitlab.com/flex_comp/protobuf2/test"
	"os"
	"strings"
	"testing"
)

func TestProtobuf(t *testing.T) {
	t.Run("protobuf", func(t *testing.T) {
		_ = comp.Init(map[string]interface{}{
			"etcd_hosts": strings.Split(os.Getenv("ETCD_HOSTS"), ","),
			"env":        os.Getenv("SRV_ENV"),
		})

		f := new(test.Foo)
		f.Foo = "foo"

		d, e := Marshal(f)
		if e != nil {
			t.Fatal(e)
		}

		p, e := UnMarshal(d)
		if e != nil {
			t.Fatal(e)
		}

		f, ok := p.(*test.Foo)
		if !ok || f.Foo != "foo" {
			t.Fatal(e)
		}
	})
}
