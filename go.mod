module gitlab.com/flex_comp/protobuf2

go 1.16

require (
	github.com/andreburgaud/crypt2go v0.13.0
	github.com/cncf/xds/go v0.0.0-20211216145620-d92e9ce0af51 // indirect
	github.com/spf13/viper v1.10.1 // indirect
	gitlab.com/flex_comp/comp v0.1.4
	gitlab.com/flex_comp/log v0.1.6 // indirect
	gitlab.com/flex_comp/remote_conf v0.1.2
	gitlab.com/flex_comp/util v0.2.6
	go.uber.org/atomic v1.9.0 // indirect
	go.uber.org/multierr v1.7.0 // indirect
	go.uber.org/zap v1.19.1 // indirect
	golang.org/x/crypto v0.0.0-20211215153901-e495a2d5b3d3 // indirect
	golang.org/x/net v0.0.0-20211216030914-fe4d6282115f // indirect
	golang.org/x/sys v0.0.0-20211216021012-1d35b9e2eb4e // indirect
	google.golang.org/protobuf v1.27.1
)
