package protobuf2

import (
	"crypto/aes"
	"crypto/cipher"
	"encoding/hex"
	"github.com/andreburgaud/crypt2go/padding"
	"gitlab.com/flex_comp/comp"
	"gitlab.com/flex_comp/remote_conf"
	"gitlab.com/flex_comp/util"
	"google.golang.org/protobuf/proto"
	"google.golang.org/protobuf/reflect/protoregistry"
	"hash/crc64"
)

var (
	pb *Protobuf
)

func init() {
	pb = &Protobuf{
		key: []byte{},
		iv:  []byte{},
	}

	_ = comp.RegComp(pb)
}

type Protobuf struct {
	key    []byte
	iv     []byte
	sum    bool
	crypto bool
}

func (p *Protobuf) Init(execArgs map[string]interface{}, _ ...interface{}) error {
	key := []byte(util.ToString(conf.Get("protobuf", "key")))
	iv := []byte(util.ToString(conf.Get("protobuf", "iv")))
	p.sum = util.ToBool(conf.Get("protobuf", "sum"))
	p.crypto = util.ToBool(conf.Get("protobuf", "crypto"))

	p.key = make([]byte, hex.DecodedLen(len(key)))
	hex.Decode(p.key, key)

	p.iv = make([]byte, hex.DecodedLen(len(iv)))
	hex.Decode(p.iv, iv)

	return nil
}

func (p *Protobuf) Start(...interface{}) error {
	return nil
}

func (p *Protobuf) UnInit() {
}

func (p *Protobuf) Name() string {
	return "protobuf"
}

func (p *Protobuf) encrypt(data []byte) ([]byte, error) {
	padder := padding.NewPkcs7Padding(aes.BlockSize)
	raw, err := padder.Pad(data)
	if err != nil {
		return nil, err
	}

	to := make([]byte, len(raw))
	block, err := aes.NewCipher(p.key)
	if err != nil {
		return nil, err
	}

	bm := cipher.NewCBCEncrypter(block, p.iv)
	bm.CryptBlocks(to, raw)
	return to, nil
}

func (p *Protobuf) decrypt(data []byte) ([]byte, error) {
	if len(data)%aes.BlockSize != 0 {
		return nil, ErrDataLenInvalid
	}

	block, err := aes.NewCipher(p.key)
	if err != nil {
		return nil, err
	}

	raw := make([]byte, len(data))
	bm := cipher.NewCBCDecrypter(block, p.iv)
	bm.CryptBlocks(raw, data)

	return padding.NewPkcs7Padding(aes.BlockSize).Unpad(raw)
}

func (p *Protobuf) calcSum(msg *Bearer) (uint64, error) {
	crc := crc64.New(crc64.MakeTable(crc64.ISO))
	_, err := crc.Write([]byte(msg.Head))
	if err != nil {
		return 0, err
	}

	_, err = crc.Write(msg.Data)
	if err != nil {
		return 0, err
	}

	return crc.Sum64(), nil
}

func Marshal(msg proto.Message) ([]byte, error) {
	return pb.Marshal(msg)
}

func (p *Protobuf) Marshal(msg proto.Message) ([]byte, error) {
	b := new(Bearer)
	b.Head = string(proto.MessageName(msg))
	b.Data, _ = proto.Marshal(msg)
	if p.sum {
		sum, err := p.calcSum(b)
		if err != nil {
			return nil, err
		}

		b.Sum = sum
	}

	raw, err := proto.Marshal(b)
	if err != nil {
		return nil, err
	}

	if !p.crypto {
		return raw, nil
	}

	return p.encrypt(raw)
}

func UnMarshal(data []byte) (proto.Message, error) {
	return pb.UnMarshal(data)
}

func (p *Protobuf) UnMarshal(data []byte) (proto.Message, error) {
	if p.crypto {
		var err error
		data, err = p.decrypt(data)
		if err != nil {
			return nil, err
		}
	}

	b := new(Bearer)
	err := proto.Unmarshal(data, b)
	if err != nil {
		return nil, err
	}

	if p.sum {
		sum, err := p.calcSum(b)
		if err != nil {
			return nil, err
		}

		if sum != b.Sum {
			return nil, ErrSumInvalid
		}
	}

	// 解业务包
	ty, err := protoregistry.GlobalTypes.FindMessageByURL(b.Head)
	if err != nil {
		return nil, err
	}

	msg := ty.New().Interface()
	err = proto.Unmarshal(b.Data, msg)
	return msg, err
}
